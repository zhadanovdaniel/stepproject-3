import { getAllCards } from "./components/API/getAllCards.js";
import { showAllCards } from "./components/showAllCards.js";



let enterBtn = document.querySelector('.enter-btn');
let confirmBtn = document.querySelector('.btn-default');
// let token = '13f6d578-941e-420e-b6d5-eaeb1f133f6b';
let email = document.querySelector('.email-input');
let password = document.querySelector('.password-input');
let globalToken = '';

// Имейл и пароль внизу

// const userEmail = 'danipop@email.com';
// const userPassword = '12345678';

let card = {}
let count = 0;

if(count == 0){
  document.querySelector('.title').innerText = 'No items have been added'
}else{
  document.querySelector(".title").style.display ="none"
}

enterBtn.addEventListener('click', function(){
    confirmBtn.addEventListener('click', function(){
    new Modal().render();

})
})


class Modal {
  constructor(){

  }
  render(){
    
    fetch("https://ajax.test-danit.com/api/v2/cards/login", {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
          },
    body: JSON.stringify({ email: `${email.value}`, password: `${password.value}` })
    })
    .then(response => response.text())
  .then(responseToken => {
  if(responseToken) {
    globalToken = responseToken
    // globalToken = response
    console.log(globalToken )
    enterBtn.remove()
    document.querySelector('.enter').innerHTML = `<button class="card-btn btn-lg btn-info" data-toggle="modal" data-target="#myCard" style="margin-top: 20px;">Створити візит</button>`
	getAllCards(globalToken);

  }


  })
  }
}


class Visit {
    constructor(purpose, description, terms, name){
        this.purpose = purpose;
        this.description = description;
        this.terms = terms;
        this.name = name;
    }
    renderCard(){
      let element = `<div id="card">
        
      <div class="modal-body">
        <input placeholder="мета" id="purpose" type="text" class="input"class="input">
      </div>
      <div class="modal-body">
        <input placeholder="опис" width="70px" height="50px" id="description" type="text" class="input">
      </div>
      
      <div class="modal-body">
          <div class="dropdown">
          <button class="btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Терміновість
          </button>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item item-urgency" href="#">Звичайна</a></li>
            <li><a class="dropdown-item item-urgency" href="#">Пріоритетна</a></li>
            <li><a class="dropdown-item item-urgency" href="#">Невідкладна</a></li>
          </ul>
      </div>


      <div class="modal-body">
        <p class="urgency"></p>
      </div>

    </div>

      <div class="modal-body">
        <input placeholder="піб" id="initials" type="text" class="input">
      </div> `     

      document.querySelector('.place').innerHTML = element;
      document.querySelectorAll('.item-urgency').forEach((i) => {
        
        i.addEventListener('click', function(){
          
          document.querySelector('.urgency').innerHTML = i.innerText;
          card.urgency = i.innerText;
          
        })
      })

      document.querySelector('#myCard').addEventListener('click', function(e){

        e.target.classList.contains('modal-body') || e.target.classList.contains('input')
        || e.target.classList.contains('item-urgency') || e.target.classList.contains('dropdown-toggle') 
        || e.target.classList.contains('dropdown') || e.target.classList.contains('urgency') ? false : 
        document.querySelectorAll('input, .urgency').forEach((i) => { i.value = null; i.innerText = ''})
  
      })



    }
    
    createCard(){
     this.renderCard();
    }
}

class VisitDentist extends Visit {
  constructor(date, ...args){
    super(...args);
    this.date = date;
  }
  renderCard(){

      let element = ` <div class="modal-body">
        <input placeholder="дата" id="date" type="date" class="input">
      </div>

      <div class="modal-body">
          <button class="create-card" data-dismiss="modal">Створити</button>
      </div>

      <div class="modal-body">
          <button class="close" data-dismiss="modal">Закрити</button>
      </div>
        
        </div>`

        document.querySelector('.else').innerHTML = element;
  }

  createCard(){

    this.renderCard();

    document.querySelector('.create-card').addEventListener('click', function(){

      count += 1;
      
      count > 0 ? document.querySelector('.title').innerText = '' : false;

      card.date = document.querySelector('#date').value;
      card.purpose = document.querySelector('#purpose').value;
      card.description = document.querySelector('#description').value;
      card.initials = document.querySelector('#initials').value;

      fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${globalToken}`
  },
  body: JSON.stringify({
    title: 'Візит до стоматолога',
    doctor: 'Стоматолог',
    purpose: card.purpose,
    description: card.description,
    urgency: card.urgency,
    initials: card.initials,
    date: card.date,
  })
})
  .then(response => response.json())
  .then(response => {
    // token = token

    card.id = response.id;

    if(card.date !== '' && card.initials !== '' && card.urgency !== undefined && card.description !== '' && card.purpose !== ''){

    document.querySelector('.card__container').insertAdjacentHTML('beforeend', `<div class="card" style="width: 20vw">
    <p id="card-${card.id}">ID: ${card.id}</p> 
    <p>Title: Візит до стоматолога</p>
    <p>Doctor: Стоматолог</p>
    <p>Purpose: ${card.purpose}</p>
    <p>Description: ${card.description}</p>
    <p>Urgency: ${card.urgency}</p>
    <p>Initials: ${card.initials}</p>
    <p>Date: ${card.date}</p></div>`)
    }
    // getAllCards(token);
  })
    })
    }
  }


class VisitCardiologist extends Visit {
  constructor(pressure, imt, diseases, age, ...args){
    super(...args)
    this.pressure = pressure;
    this.imt = imt;
    this.diseases = diseases;
    this.age = age;
  }
  renderCard(){

      let element = `<div class="modal-body">
        <input placeholder="тиск" id="pressure" type="text" class="input">
      </div>

      <div class="modal-body">
        <input placeholder="імт" id="wh" type="text" class="input">
      </div>

      <div class="modal-body">
        <input placeholder="захворювання" id="diseases" type="text" class="input">
      </div>

      <div class="modal-body">
        <input placeholder="вік" id="age" type="text" class="input">
      </div>

      <div class="modal-body">
          <button class="create-card" data-dismiss="modal">Створити</button>
      </div>

      <div class="modal-body">
          <button class="close" data-dismiss="modal">Закрити</button>
      </div>
        
        </div>`

        document.querySelector('.else').innerHTML = element;

  }
  createCard(){

  this.renderCard();

  document.querySelector('.create-card').addEventListener('click', function(){

    count += 1;
    
    count > 0 ? document.querySelector('.title').innerText = '' : false;

    card.age = document.querySelector('#age').value;
    card.diseases = document.querySelector('#diseases').value;
    card.imt = document.querySelector('#wh').value;
    card.pressure = document.querySelector('#pressure').value;
    card.purpose = document.querySelector('#purpose').value;
    card.description = document.querySelector('#description').value;
    card.initials = document.querySelector('#initials').value;

      fetch("https://ajax.test-danit.com/api/v2/cards", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${globalToken}`
    },
    body: JSON.stringify({
      title: 'Візит до кардіолога',
      doctor: 'Кардіолог',
      purpose: card.purpose,
      description: card.description,
      urgency: card.urgency,
      initials: card.initials,
      pressure: card.pressure,
      bmi: card.imt,
      diseases: card.diseases,
      age: card.age
    })
  })
    .then(response => response.json())
    .then(response => {
      // token = token
     card.id = response.id;

     if(card.age !== '' && card.initials !== '' && card.urgency !== undefined && card.description !== '' && card.purpose !== '' && card.pressure !== '' && card.bmi !== '' && card.diseases !== ''){

     document.querySelector('.card__container').insertAdjacentHTML('beforeend', `<div class="card" style="width: 20vw">
     <p id="card-${card.id}">ID: ${card.id}</p> 
     <p>Title: Візит до кардіолога</p>
     <p>Doctor: Кардіолог</p>
     <p>Purpose: ${card.purpose}</p>
     <p>Description: ${card.description}</p>
     <p>Urgency: ${card.urgency}</p>
     <p>Initials: ${card.initials}</p>
     <p>Pressure: ${card.pressure}</p>
     <p>Імт: ${card.bmi}</p>
     <p>Diseases: ${card.diseases}</p>
     <p>Age: ${card.age}</p></div>`)

     }
      // getAllCards(token);
    })

      })

    
  }
  }



class VisitTherapist extends Visit {
  constructor(age, ...args){
    super(...args)
    this.age = age;
  }
  renderCard(){
        
      let element = `<div class="modal-body">
        <input placeholder="вік" id="age" type="text" class="input">
      </div>
    
      <div class="modal-body">
          <button class="create-card" data-dismiss="modal">Створити</button>
      </div>

      <div class="modal-body">
          <button class="close" data-dismiss="modal">Закрити</button>
      </div>`

        document.querySelector('.else').innerHTML = element

      }

  createCard(){

    this.renderCard();

    document.querySelector('.create-card').addEventListener('click', function(){

      count += 1;

      count > 0 ? document.querySelector('.title').innerText = '' : false;

      card.age = document.querySelector('#age').value;
      card.purpose = document.querySelector('#purpose').value;
      card.description = document.querySelector('#description').value;
      card.initials = document.querySelector('#initials').value;

      fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${globalToken}`
  },
  body: JSON.stringify({
    title: 'Візит до терапевта',
    doctor: 'Терапевт',
    purpose: card.purpose,
    description: card.description,
    urgency: card.urgency,
    initials: card.initials,
    age: card.age
  })
})
  .then(response => response.json())
  .then(response => {
// token = token
    card.id = response.id;

    if(card.age !== '' && card.initials !== '' && card.urgency !== undefined && card.description !== '' && card.purpose !== ''){
  
    document.querySelector('.card__container').insertAdjacentHTML('beforeend', `<div class="card" style="width: 20vw">
          <p id="card-${card.id}">ID: ${card.id}</p> 
          <p>Title: Візит до терапевта</p>
          <p>Doctor: Терапевт</p>
          <p>Purpose: ${card.purpose}</p>
          <p>Description: ${card.description}</p>
          <p>Urgency: ${card.urgency}</p>
          <p>Initials: ${card.initials}</p>
          <p>Age: ${card.age}</p></div>`)

    }
    // getAllCards(token);
  })


    })
       
  }

}

document.querySelectorAll('.dropdown-item').forEach((i) => i.addEventListener('click', function(e){

  if(e.target == document.querySelector('.cardiologist')){

    new Visit().createCard()
    new VisitCardiologist().createCard()

  } else if(e.target == document.querySelector('.dentist')){

    new Visit().createCard()
    new VisitDentist().createCard()

  } else if(e.target == document.querySelector('.therapist')){

    new Visit().createCard()
    new VisitTherapist().createCard()

  }
} ))
// export {token};
// filter()